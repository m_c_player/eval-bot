const mf = require('mineflayer');

const bot = mf.createBot({
    host: process.argv[2],
    port: parseInt(process.argv[3]),
    username: "EvalBot"
});

bot.on('chat', (uname, msg, jsonMsg) => {
    if(msg.startsWith("/eval")) {
        try { bot.chat(`${eval(`${msg}`.slice("/eval ".length))}`); } catch(err) { bot.chat(`${err}`); };
    }
    console.log(msg)
});